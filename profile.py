"""Profile with a single PC running Ubuntu 20.04 LTS.

Instructions:
Wait for the profile instance to start, then click on the node in the topology and choose the `shell` menu item. 
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the emulab extensions library.
import geni.rspec.emulab

# Resource strings
#PCIMG = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD'
PCIMG = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD'

PCHWTYPE_SEL = [("d430", "D430 - Min"),
                ("d740", "D740 - Mid"),
                ("d840", "D840 - Max")]

# Create a portal context, needed to defined parameters
pc = portal.Context()

pc.defineParameter("pchwtype", "PC Hardware Type",
                   portal.ParameterType.IMAGE,
                   PCHWTYPE_SEL[2], PCHWTYPE_SEL,
                   longDescription="Select the PC Hardware Type")

pc.defineParameter("mountpt", "Mountpoint for 3rd party libraries",
                   portal.ParameterType.STRING, "/opt")

#pc.defineParameter("datasetmountpt", "Dataset Mountpoint (Hdf5)",
#                   portal.ParameterType.STRING, "/scratch/renew_datasets")

pc.defineParameter("fixedid", "Fixed PC Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc1' to this specific node.  Leave blank to allow for any available node of the correct type.")
				   
# Bind and verify parameters.
params = pc.bindParameters()
pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

pc1 = request.RawPC("pc1")
if params.fixedid:
    pc1.component_id=params.fixedid
else:
    pc1.hardware_type = params.pchwtype
pc1.disk_image = PCIMG

bs = pc1.Blockstore("intellibbs", params.mountpt)
bs.size = "32GB"
# any|sysvol|nonsysvol
bs.placement = "nonsysvol"

#bs = pc1.Blockstore("datasets", params.datasetmountpt)
#bs.size = "4GB"
# any|sysvol|nonsysvol
#bs.placement = "nonsysvol"

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
